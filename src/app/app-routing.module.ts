import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageGamesComponent } from './games/manage-games/manage-games.component';
import { NoGameComponent } from './games/manage-games/no-game';
import { NewGameComponent } from './new-game/new-game.component';
import { GameDetailsComponent } from './games/manage-games/game-details/game-details.component';
import { NotFoundComponent } from './not-found.component';
import { PlatformsComponent } from './games/manage-games/platforms/platforms.component';

const routes: Routes = [
  {path: 'new', component: NewGameComponent},
  {path: 'platforms', component: PlatformsComponent},
  {path: 'games', component: ManageGamesComponent, children: [
      {path: '', component: NoGameComponent},
      {path: ':id', component: GameDetailsComponent},
    ]},
  {path: '**', component: NotFoundComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
