import { Game } from './game.model';
import { EventEmitter } from '@angular/core';
import { Platform } from './platform.model';

export class GameService {
  gamesChange = new EventEmitter<Game[]>();
  platformsChange = new EventEmitter<Platform[]>();
  games: Game[] = [
    new Game('Uno', 'https://www.mattelgames.com/content/dam/toy-box/mattel-games/cards/uno/uno_icon_500x500r.jpg', 'NES', 'family game'),
    new Game('Jumanji', 'https://www.slotozilla.com/wp-content/uploads/sites/12001/Jumanji.jpg', 'NES', 'kids game'),
    new Game('World of WarCraft', 'https://upload.wikimedia.org/wikipedia/en/thumb/9/91/WoW_Box_Art1.jpg/220px-WoW_Box_Art1.jpg', 'Super Nintendo', 'role-playing game'),
    new Game('The Sunrise Village', 'https://www.innogames.com/fileadmin/user_upload/images/games/Sunrise_Village/1280x720_-_Screenshot_column_4.jpg', 'Sega Genesis', 'kids game'),
  ];

  platforms: Platform[] = [
    new Platform('NES'),
    new Platform('Sega Genesis'),
    new Platform('Super Nintendo'),
  ];

  getPlatforms() {
    return this.platforms.slice();
  }

  getGames() {
    return this.games.slice();
  }
  getGame(index: number) {
    return this.games[index];
  }

  addGame(game: Game) {
    this.games.push(game);
    this.gamesChange.emit(this.games);
  }

  // getGamesByPlatform(platform: string, game:Game) {
  //   this.games.forEach(
  //     // if (this.platform === 'NES') {
  //     //   this.platform.push(game)
  //   })
  // }
}
