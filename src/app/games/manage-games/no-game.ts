import { Component } from '@angular/core';
@Component({
  selector: 'app-no-game',
  template: `
    <h4>Game details</h4>
    <p>No game is selected</p>
  `,
})
export class NoGameComponent {}
