import { Component, OnInit } from '@angular/core';
import { Game } from '../../../shared/game.model';
import { GameService } from '../../../shared/game.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css']
})
export class GameDetailsComponent implements OnInit {
  game!: Game;

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      const gameId: number = parseInt(params['id']);
      this.game = this.gameService.getGame(gameId);
    });
  }

}
