import { Component, OnInit } from '@angular/core';
import { Game } from '../../shared/game.model';
import { GameService } from '../../shared/game.service';

@Component({
  selector: 'app-manage-games',
  templateUrl: './manage-games.component.html',
  styleUrls: ['./manage-games.component.css']
})
export class ManageGamesComponent implements OnInit {
  games!: Game[];

  constructor(private gameService: GameService) { }

  ngOnInit(){
    this.games = this.gameService.getGames();
    this.gameService.gamesChange.subscribe((games: Game[]) => {
      this.games = games;
    });
  }
}
