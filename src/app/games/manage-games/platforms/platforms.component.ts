import { Component, OnInit } from '@angular/core';
import { GameService } from '../../../shared/game.service';
import { Platform } from '../../../shared/platform.model';

@Component({
  selector: 'app-platforms',
  templateUrl: './platforms.component.html',
  styleUrls: ['./platforms.component.css']
})
export class PlatformsComponent implements OnInit {
  platforms!: Platform[];

  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.platforms = this.gameService.getPlatforms();
    this.gameService.platformsChange.subscribe((platforms: Platform[]) => {
      this.platforms = platforms;
    });
  }
}
