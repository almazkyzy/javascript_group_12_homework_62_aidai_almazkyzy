import { Component, ElementRef, ViewChild } from '@angular/core';
import { GameService } from '../shared/game.service';
import { Game } from '../shared/game.model';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('imageUrlInput') imageUrlInput!: ElementRef;
  @ViewChild('descriptionInput') descriptionInput!:ElementRef
  platform: string= '';

  constructor(public gameService: GameService) {
  }

  createGame() {
    const name = this.nameInput.nativeElement.value;
    const description = this.descriptionInput.nativeElement.value;
    const imageUrl = this.imageUrlInput.nativeElement.value;
    const platform = this.platform
    const game = new Game(name, imageUrl, platform, description);
    this.gameService.addGame(game);
  }
}
