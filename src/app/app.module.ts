import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewGameComponent } from './new-game/new-game.component';
import { FormsModule } from '@angular/forms';
import { GameItemComponent } from './games/game-item/game-item.component';
import { GamesComponent } from './games/games.component';
import { GameService } from './shared/game.service';
import { ManageGamesComponent } from './games/manage-games/manage-games.component';
import { GameDetailsComponent } from './games/manage-games/game-details/game-details.component';
import { AppRoutingModule } from './app-routing.module';
import { PlatformsComponent } from './games/manage-games/platforms/platforms.component';
import { NoGameComponent } from './games/manage-games/no-game';


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewGameComponent,
    GameItemComponent,
    GamesComponent,
    ManageGamesComponent,
    GameDetailsComponent,
    PlatformsComponent,
    NoGameComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
